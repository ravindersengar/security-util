package com.xlrs.security.security;

public class UserInContextFactory {

	public static UserInContext getUserInContext(String username, String name,
			String userRoles, Long orgUserId, String email) {
		
		return new UserInContext(username, name, orgUserId, email, userRoles);
	}

}

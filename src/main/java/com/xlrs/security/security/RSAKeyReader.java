package com.xlrs.security.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.springframework.beans.factory.annotation.Value;

//@Component
public class RSAKeyReader {

	private RSAPrivateKey privateKey;

	private RSAPublicKey publicKey;

	public RSAKeyReader(
			@Value("token.privateKey") String privateKeyStr, 
			@Value("token.privateKey") String publicKeyStr) throws IOException {
		privateKey = (RSAPrivateKey) loadPrivatekey(readKey("Private", privateKeyStr), "RSA");
		publicKey = (RSAPublicKey) loadPublickey(readKey("Public", publicKeyStr), "RSA");
	}

	public RSAPrivateKey getPrivateKey() {
		return privateKey;
	}

	public RSAPublicKey getPublicKey() {
		return publicKey;
	}
	
	private byte[] readKey(String type, String key) throws IOException {
		PemReader reader = null;
		try {
			reader = new PemReader(new BufferedReader(new StringReader(key)));
			PemObject pemObject = reader.readPemObject();
			return pemObject.getContent();
		}finally {
			reader.close();
		}
	}

	private static PublicKey loadPublickey(byte[] keyBytes, String algorithm) {
		PublicKey publicKey;
		try {
			KeyFactory kf = KeyFactory.getInstance(algorithm);
			EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
			publicKey = kf.generatePublic(keySpec);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException("Unable to create public key", e);
		}
		return publicKey;
	}
	
	private PrivateKey loadPrivatekey(byte[] keyBytes, String algorithm) {
		PrivateKey privateKey;
		try {
			KeyFactory kf = KeyFactory.getInstance(algorithm);
			EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
			privateKey = kf.generatePrivate(keySpec);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException("Unable to create private key", e);
		}
		return privateKey;
	}
}

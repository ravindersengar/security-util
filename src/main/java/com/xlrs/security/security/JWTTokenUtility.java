package com.xlrs.security.security;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.util.EncryptionUtil;
import com.xlrs.security.constant.AuthenticationConstants;
import com.xlrs.security.exception.AuthTokenMissingException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;

@Component("jwtTokenUtility")
public class JWTTokenUtility implements Serializable {
	
	/*private Key privateKey = null;
	private Key publicKey = null;
	
	final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.RS256;
	
	public JWTTokenUtility(RSAKeyReader rsaKeyReader) {
		publicKey = rsaKeyReader.getPublicKey();
		privateKey = rsaKeyReader.getPrivateKey();
	}*/
	
	private static final long serialVersionUID = -3301605591108950415L;
	private Clock clock = DefaultClock.INSTANCE;

	public static final ObjectMapper objectMapper = new ObjectMapper();

	public static final TypeReference<UserInContext> userInContextType = new TypeReference<UserInContext>() {
	};
	

	public String getJWTTokenFromRequest(HttpServletRequest req) throws Exception {
		
		String authToken = EncryptionUtil.decrypt(req.getHeader(AuthenticationConstants.JWT_HEADER)) ;
		if (authToken == null || null == authToken.substring(7)) {
			throw new AuthTokenMissingException("No Authentication token found in Header");
		}
		String token = authToken.substring(7);
		if(token==null || token.length()<60) {
			throw new ApplicationException("Authentication token invalid or missing");
		}
		return authToken.substring(7);
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(AuthenticationConstants.JWT_SECRET_KEY).parseClaimsJws(token).getBody();
	}

	public String generateToken(UserInContext userContext) throws Exception {
		Map<String, Object> claims = new HashMap<>();
		String token = doGenerateToken(claims, userContext, AuthenticationConstants.JWT_EXPIRY_IN_MINUTES);
		return EncryptionUtil.encrypt(token);
	}

	private String doGenerateToken(Map<String, Object> claims, UserInContext userContext, Integer expiryTime) throws JsonProcessingException, ApplicationException {
		try {
			final Date createdDate = clock.now();
			final Date expirationDate = calculateExpirationDate(createdDate, expiryTime);
			String userContextPayload = objectMapper.writeValueAsString(userContext);
		
			return Jwts.builder()
				.setClaims(claims)
				.setSubject(userContextPayload)
				.setIssuedAt(createdDate)
				.setExpiration(expirationDate)
				//.signWith(signatureAlgorithm, privateKey)
				.signWith(SignatureAlgorithm.HS512, AuthenticationConstants.JWT_SECRET_KEY)
				.compact();
		} catch (Exception e) {
			throw new ApplicationException("Token generation failed. Either credentials are invalid or tempared");
		}
	}

	public String refreshToken(String token) throws Exception {
		final Date createdDate = clock.now();
		final Date expirationDate = calculateExpirationDate(createdDate, AuthenticationConstants.JWT_EXPIRY_IN_MINUTES);

		final Claims claims = getAllClaimsFromToken(token);
		claims.setIssuedAt(createdDate);
		claims.setExpiration(expirationDate);

		String refreshedToken = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, AuthenticationConstants.JWT_SECRET_KEY).compact();
		return EncryptionUtil.encrypt(refreshedToken);
	}

	public String generateAccActivationToken(String token) throws Exception {
		final Date createdDate = clock.now();
		final Date expirationDate = calculateExpirationDate(createdDate, AuthenticationConstants.JWT_EXPIRY_FOR_ACC_ACTIVATION);

		final Claims claims = getAllClaimsFromToken(token);
		claims.setIssuedAt(createdDate);
		claims.setExpiration(expirationDate);

		String accActivationToken = Jwts.builder().setClaims(claims)//.signWith(signatureAlgorithm, privateKey)
				.signWith(SignatureAlgorithm.HS512, AuthenticationConstants.JWT_SECRET_KEY)
				.compact();
		return EncryptionUtil.encrypt(accActivationToken);
	}

	public String generatePasswordResetToken(UserInContext userContext) throws Exception {
		Map<String, Object> claims = new HashMap<>();
		final Date createdDate = clock.now();
		final Date expirationDate = calculateExpirationDate(createdDate, AuthenticationConstants.JWT_EXPIRY_FOR_FORGET_PASSWORD);
		String userContextPayload = objectMapper.writeValueAsString(userContext);

		String passwordResetToken = Jwts.builder()
				.setClaims(claims)
				.setSubject(userContextPayload)
				.setIssuedAt(createdDate)
				.setExpiration(expirationDate)
				//.signWith(signatureAlgorithm, privateKey)
				.signWith(SignatureAlgorithm.HS512, AuthenticationConstants.JWT_SECRET_KEY)
				.compact();
		return EncryptionUtil.encrypt(passwordResetToken);
	}

	public UserInContext getUserContextFromToken(String token) throws IOException {
		String userContextPayload = getClaimFromToken(EncryptionUtil.decrypt(token), Claims::getSubject);

		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		UserInContext userContext = objectMapper.readValue(userContextPayload, userInContextType);
		return userContext;

	}

	public String getUserIdFromToken(String token) throws Exception {
		return getClaimFromToken(EncryptionUtil.decrypt(token), Claims::getId);
	}

	public Date getIssuedAtDateFromToken(String token) throws Exception {
		return getClaimFromToken(EncryptionUtil.decrypt(token), Claims::getIssuedAt);
	}

	public Date getExpirationDateFromToken(String token) throws Exception {
		return getClaimFromToken(EncryptionUtil.decrypt(token), Claims::getExpiration);
	}

	private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
		return (lastPasswordReset != null && created.before(lastPasswordReset));
	}

	private Boolean isTokenExpired(String token) throws Exception {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(clock.now());
	}

	private Boolean ignoreTokenExpiration(String token) {
		// here you specify tokens, for that the expiration is ignored
		return false;
	}

	public Boolean canTokenBeRefreshed(String token, Date lastPasswordReset)throws Exception {
		final Date created = getIssuedAtDateFromToken(token);
		return !isCreatedBeforeLastPasswordReset(created, lastPasswordReset) && (!isTokenExpired(token) || ignoreTokenExpiration(token));
	}

	public Boolean isValidToken(String token) throws Exception {
		return (!isTokenExpired(token));
	}

	private Date calculateExpirationDate(Date createdDate, Integer expiryTime) {
		return new Date(createdDate.getTime() + (expiryTime * (60 * 1000)));
	}

	
	
	
}

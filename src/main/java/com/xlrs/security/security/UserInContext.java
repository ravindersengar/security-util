package com.xlrs.security.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
public class UserInContext extends RequestContext{
	
	private static final long serialVersionUID = 1L;
	private String username;
	private String name;
	private Long orgUserId;
	private String email;
	private String roles;
}

package com.xlrs.security.aspect;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.xlrs.commons.util.CommonUtil;
import com.xlrs.security.constant.AuthenticationConstants;
import com.xlrs.security.security.JWTTokenUtility;
import com.xlrs.security.security.UserInContext;
import com.xlrs.security.util.ThreadAttribute;

import io.jsonwebtoken.JwtException;

@Aspect
@Configuration
public class AuthenticationInterceptorAspect  {

	private static List<String> excludedURLs = Arrays.asList("/login","/auth/access-token", "/error",  "/actuator/*","/mailer/email",
		"/registration/sendResetPasswordMail","/registration/confirmRegistration","/registration/resetPassword", "/registration/sendResetPasswordMail");

	@Autowired
	JWTTokenUtility jwtTokenUtility;

	@Before("bean(*Controller)")
	public void before(JoinPoint joinPoint) throws JwtException {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String authToken = null;
		String loggedInUserId = CommonUtil.randomString(5);
		String reqTransactionId = request.getHeader(AuthenticationConstants.REQ_TRANSACTION_ID);
		if(!excludedURLs.contains(request.getRequestURI())) {
			String requestHeader = request.getHeader(AuthenticationConstants.JWT_HEADER);
			
			if (requestHeader != null && requestHeader.startsWith(AuthenticationConstants.JWT_BEARER_PREFIX)) {
				authToken = requestHeader.substring(7);
				try {
					if (!jwtTokenUtility.isValidToken(authToken)) {
						throw new JwtException("Invalid JWT Token");
					} else {
						UserInContext userInContext = jwtTokenUtility.getUserContextFromToken(authToken);
						loggedInUserId = userInContext.getOrgUserId().toString();
						ThreadAttribute.setUserContext(userInContext);
						ThreadAttribute.setSecurityToken(authToken);
					}
				} catch (Exception e) {
					throw new JwtException("Invalid JWT Token", e);
				}
			} else {
				throw new JwtException("Authentication credentials/token missing");
			}
		}
		if(reqTransactionId!=null) {
			ThreadAttribute.setReqTransactionId(reqTransactionId);
		}else {
			ThreadAttribute.setReqTransactionId(loggedInUserId+ZonedDateTime.now().toInstant().toEpochMilli() + "");
		}
	}
}

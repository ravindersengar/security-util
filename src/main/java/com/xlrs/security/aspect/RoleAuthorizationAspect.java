package com.xlrs.security.aspect;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;

import com.xlrs.security.security.AuthorizeRoles;
import com.xlrs.security.util.ThreadAttribute;

import io.jsonwebtoken.JwtException;

@Aspect
@Configuration
public class RoleAuthorizationAspect {

	@Before("@annotation(com.xlrs.security.security.AuthorizeRoles)")
	public void before(JoinPoint joinPoint) throws JwtException {
		String usersRoles = ThreadAttribute.getUserInContext().getRoles();
		boolean isAllowed =false;
		if(usersRoles!=null) {
			
			MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		    Method method = signature.getMethod();

		    AuthorizeRoles roles = method.getAnnotation(AuthorizeRoles.class);
		    String[] allowedRoles = roles.values();
			for(String assignedRole:allowedRoles) {
				if(usersRoles.equals(assignedRole)) {
					isAllowed=true;
					break;
				}
			}
		}
		if(!isAllowed) {
			throw new RuntimeException("User is not authorized.");
		}
	}
}

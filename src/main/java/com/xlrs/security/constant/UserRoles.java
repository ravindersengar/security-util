package com.xlrs.security.constant;

public final class UserRoles {

	public static final String ROLE_USER = "ROLE_USER";

	public static final String ROLE_MARKETPLACE_ADMIN = "ROLE_MARKETPLACE_ADMIN";

	public static final String ROLE_VENDOR_ADMIN = "ROLE_VENDOR_ADMIN";
	
	public static final String ROLE_ADMIN = "ROLE_ADMIN";

	public static final String getDefaultUserRole() {
		return UserRoles.ROLE_USER;
	}

}

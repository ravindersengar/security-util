package com.xlrs.security.constant;

public class AuthenticationConstants {
	
	public static final Integer JWT_EXPIRY_IN_MINUTES = 30;
	
	public static final Integer JWT_EXPIRY_FOR_ACC_ACTIVATION = 300;
	
	public static final Integer JWT_EXPIRY_FOR_FORGET_PASSWORD = 30;
	
	public static final String JWT_HEADER = "Authorization";
	
	public static final String JWT_BEARER_PREFIX = "Bearer";
	
	public static final String JWT_SECRET_KEY = "185318c0dbaee3ebc7e24986223eb1b4";

	public static final String REQ_TRANSACTION_ID = "ReqTransactionId";
	
	
}

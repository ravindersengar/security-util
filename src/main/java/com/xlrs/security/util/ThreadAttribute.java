package com.xlrs.security.util;

import com.xlrs.security.security.UserInContext;

public class ThreadAttribute {
	private static InheritableThreadLocal<UserInContext> threadAttrs = new InheritableThreadLocal<UserInContext>() {
		@Override
		protected UserInContext initialValue() {
			return new UserInContext();
		}
	};

	public static void setReqTransactionId(String reqTranxId) {
		threadAttrs.get().setRequestTransactionId(reqTranxId);
	}

	public static String getReqTransactionId() {
		return threadAttrs.get().getRequestTransactionId();
	}
	
	public static void setSecurityToken(String securityToken) {
		threadAttrs.get().setSecurityToken(securityToken);
	}

	public static String getSecurityToken() {
		return threadAttrs.get().getSecurityToken();
	}

	public static void setUserContext(UserInContext userInContext) {
		threadAttrs.set(userInContext);
	}

	public static UserInContext getUserInContext() {
		UserInContext userInContext = threadAttrs.get();
		if(userInContext==null || userInContext.getUsername()==null || userInContext.getOrgUserId()==null) {
			return null;
		}
		return userInContext;
	}
	
}
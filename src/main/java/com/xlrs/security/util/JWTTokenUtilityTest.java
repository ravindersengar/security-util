package com.xlrs.security.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.security.security.JWTTokenUtility;
import com.xlrs.security.security.UserInContext;

public class JWTTokenUtilityTest {
	
	public static void main(String[] args) throws JsonProcessingException, ApplicationException {
		String token = "";
		
		JWTTokenUtility utility = new JWTTokenUtility();
		
		try {
			token=utility.generateToken(new UserInContext("abc@123","RAvinder Singh",1L,"abc@gmail.com","ADMIN"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("MarketPlace Admin --> "+token);
		
	}

}

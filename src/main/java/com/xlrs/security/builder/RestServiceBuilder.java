package com.xlrs.security.builder;

import java.util.Arrays;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.xlrs.security.constant.AuthenticationConstants;
import com.xlrs.security.util.ThreadAttribute;

@Component
public class RestServiceBuilder<T> {

	private static final RestTemplate restTemplate = new RestTemplate();

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public T post(String url, Object requestObject, Class<T> responseType) {
		HttpHeaders headers = new HttpHeaders();
		setSecurityHeaders(headers);
		HttpEntity<T> request = new HttpEntity(requestObject, headers);
		T response = restTemplate.postForObject(url, request, responseType);
		return response;
	}
	
	public T postWithHeaders(String url, Object requestObject, Map<String, String> headers, Class<T> responseType) {
		HttpHeaders tempHeader = new HttpHeaders();
		headers.forEach((key, value) -> {
			tempHeader.set(key, value);
		});
		setSecurityHeaders(tempHeader);
		HttpEntity<Object> entity = new HttpEntity<>(requestObject, tempHeader);
		T response = restTemplate.postForObject(url, entity, responseType);
		return response;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public T postWithoutOAuth(String url, Object requestObject, Class<T> responseType) {
		HttpEntity<T> request = new HttpEntity(requestObject);
		T response = restTemplate.postForObject(url, request, responseType);
		return response;
	}
	
	public T get(String url, Class<T> t, Object... params) {
		HttpHeaders headers = new HttpHeaders();
		return getWithHeader(url, t, headers, params);
	}
	
	public T getWithHeader(String url, Class<T> t, HttpHeaders headers, Object... params) {
		if(headers==null) {
			headers = new HttpHeaders();
		}
		setSecurityHeaders(headers);
		HttpEntity<Void> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, t, params);
		return response.getBody();
	}
	
	private void setSecurityHeaders(HttpHeaders tempHeader) {
		String securityToken = null;
		if(ThreadAttribute.getUserInContext()!=null && ThreadAttribute.getUserInContext().getSecurityToken()!=null) {
			securityToken = ThreadAttribute.getUserInContext().getSecurityToken();
			tempHeader.add(HttpHeaders.AUTHORIZATION, AuthenticationConstants.JWT_BEARER_PREFIX + " " + securityToken);
			tempHeader.add(AuthenticationConstants.REQ_TRANSACTION_ID,
					ThreadAttribute.getUserInContext().getRequestTransactionId());
		}
		tempHeader.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		tempHeader.setContentType(MediaType.APPLICATION_JSON);
	}

}

package com.xlrs.security.exception;

public final class AuthTokenMissingException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public AuthTokenMissingException() {
        super();
    }

    public AuthTokenMissingException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AuthTokenMissingException(final String message) {
        super(message);
    }

    public AuthTokenMissingException(final Throwable cause) {
        super(cause);
    }

}

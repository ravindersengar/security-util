package com.xlrs.security.exception;

public class AuthorizationException extends RuntimeException {

	public AuthorizationException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3830852423408878547L;

}

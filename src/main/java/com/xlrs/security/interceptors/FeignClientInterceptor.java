package com.xlrs.security.interceptors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.xlrs.security.constant.AuthenticationConstants;
import com.xlrs.security.util.ThreadAttribute;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Component
public class FeignClientInterceptor implements RequestInterceptor {

	@Override
	public void apply(RequestTemplate requestTemplate) {

		String securityToken = null;
		if(ThreadAttribute.getUserInContext()!=null && ThreadAttribute.getUserInContext().getSecurityToken()!=null) {
			securityToken = ThreadAttribute.getUserInContext().getSecurityToken();
			requestTemplate.header(HttpHeaders.AUTHORIZATION, AuthenticationConstants.JWT_BEARER_PREFIX + " " + securityToken);
			requestTemplate.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
			requestTemplate.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		}
	}
	
	
}
